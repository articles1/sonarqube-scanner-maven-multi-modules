package fr.web.expose;

import fr.domain.bean.Building;

public class Expose {

    public Building getBuilding() {
        Building building = new Building("foo");
        building.setId("bar");
        return building;
    }
}
