package fr.web.expose;

import fr.domain.bean.Building;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ExposeTest {

    @Test
    public void shouldCreateBuilding() {
        // Given
        Expose expose = new Expose();

        // When
        Building building = expose.getBuilding();

        // Then
        Assertions.assertEquals("bar", building.getId());
    }

}