package fr.domain.bean;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BuildingTest {

    @Test
    public void shouldCreateBuilding() {
        // Then
        Assertions.assertEquals("foo_id", new Building("foo_id").getId());
    }
}